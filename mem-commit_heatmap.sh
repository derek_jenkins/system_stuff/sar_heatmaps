#!/bin/bash
# filename: mem-commit_heatmap.sh
# use: prints a memory commit heatmap.
# maintainer -- derektjenkins@gmail.com
# # # # #
print_header() {
    echo "=================================================="
    echo "============= memory commit heatmap =============="
    echo "=================================================="
}
create_time_column_for_heatmap() {
    for TIME_INCREMENT in {1..143}; do
        date -u -d"12am+$((${TIME_INCREMENT}*10))mins" +"%I:%M%p%t" >> mem-commit_heatmap.time_column1
    done
    # sed -i "1iTime:" mem-commit_heatmap.time_column1
    unset TIME_INCREMENT
}
create_time_column_plus_na() {
    # we use this in case sar has gaps in reporting. we call it when we create the "SA_FILE" reports.
    for TIME_INCREMENT in {1..143}; do
        date -u -d"12am+$((${TIME_INCREMENT}*10))mins" +"%I:%M%p%tN/A" >> mem-commit_heatmap.time_column1_plus_na
    done
    unset TIME_INCREMENT
}
user_selects_length_of_heatmap() {
    echo
    read -p "How many days do you want to include in this heatmap? " USER_SELECTED_DAYS
    echo
    TOTAL_DAYS=$((${USER_SELECTED_DAYS}-1))
    unset USER_SELECTED_DAYS
}
# These next few funcs are used in create_heatmap_for_each_day_of_inquiry() (where $SA_FILE gets defined).
the_sar_command() {
    sar -r -f "$(date -d "${SA_FILE} days ago" +/var/log/sa/sa%d)"
}
awk_that_gets_the_required_column() {
    # this awk script simply tests input to ensure that its output contains only rows whose values on columns $1 and $6 begin with timestamps or digits. This effectively removes the header and footer that sar provides (so that our output only contains the values for the resources we're interested in).
    awk '$1 ~ /^[0-2][0-9]:/ && $6 ~ /^[0-9]/ \
    {
        print $1, $2, $9
    }'
}
properly_format_the_required_column() {
    sed 's/:/ /g' | awk '{print $1"  " $2, $4, $5}' | sed -e 's/  /:/g' -e 's/ AM/AM/g' -e 's/ PM/PM/g'
}
join_the_sar_data_with_time_column_plus_na() {
    # When we print $2 at the end, we have a single column with the sar data or "N/A" (where sar didn't report).
    awk -F" " 'FNR==NR{a[$1]=$0;next}{if($1 in a){print a[$1];} else {print;}}' mem-commit_heatmap.sa_date_"${SA_FILE}" mem-commit_heatmap.time_column1_plus_na | awk '{print $2}'
}
awk_that_colorizes_each_day_of_inquiry() {
    # May choose to use this again if I can figure out a usable header.
    # else if ($1 ~ "Day")
    #     {
    #         printf reset_color
    #     }
    # Since red is the only non-bold color here, consider using all non-bold colors.
    local HEATMAP_BLUE="\033[1;34m"
    local HEATMAP_GREEN="\033[1;32m"
    local HEATMAP_YELLOW="\033[1;33m"
    local HEATMAP_ORANGE="\033[1;31m"
    local HEATMAP_RED="\033[0;31m"
    local HEATMAP_RESET_COLOR="\033[0;0m"
    local AWK_COLOR_INCREMENT_AMOUNT_ONE=15
    local AWK_COLOR_INCREMENT_AMOUNT_TWO=33
    local AWK_COLOR_INCREMENT_AMOUNT_THREE=55
    local AWK_COLOR_INCREMENT_AMOUNT_FOUR=80
    awk -v color_increment_amount_one="$AWK_COLOR_INCREMENT_AMOUNT_ONE" \
        -v blue="$HEATMAP_BLUE" \
        -v color_increment_amount_two="$AWK_COLOR_INCREMENT_AMOUNT_TWO" \
        -v green="$HEATMAP_GREEN" \
        -v color_increment_amount_three="$AWK_COLOR_INCREMENT_AMOUNT_THREE" \
        -v yellow="$HEATMAP_YELLOW" \
        -v color_increment_amount_four="$AWK_COLOR_INCREMENT_AMOUNT_FOUR" \
        -v orange="$HEATMAP_ORANGE" \
        -v red="$HEATMAP_RED" \
        -v reset_color="$HEATMAP_RESET_COLOR" \
        '{
            if ($1 == "N/A")
            {
                printf reset_color
            }
        else if ($1 <= color_increment_amount_one)
            {
                printf blue
            }
        else if ($1 <= color_increment_amount_two)
            {
                printf green
            }
        else if ($1 <= color_increment_amount_three)
            {
                printf yellow
            }
        else if ($1 <= color_increment_amount_four)
            {
                printf orange
            }
        else if ($1 > color_increment_amount_four)
            {
                printf red
            }
            print $1 reset_color
        }' mem-commit_heatmap_data-with-na.sa_date_"${SA_FILE}" > mem-commit_heatmap.sa_date_"${SA_FILE}"
}
create_heatmap_for_each_day_of_inquiry() {
    # we use create_time_column_plus_na() so that we wind up with "N/A" fields where sar wasn't reporting. we use the file this func creates to join with each SA_FILE.
    create_time_column_plus_na
    for SA_FILE in $(eval echo {$TOTAL_DAYS..0}); do
        the_sar_command | awk_that_gets_the_required_column | properly_format_the_required_column > mem-commit_heatmap.sa_date_"${SA_FILE}"
        join_the_sar_data_with_time_column_plus_na > mem-commit_heatmap_data-with-na.sa_date_"${SA_FILE}"
        # sed -i "1iDay_${SA_FILE}" mem-commit_heatmap_data-with-na.sa_date_"${SA_FILE}"
        awk_that_colorizes_each_day_of_inquiry
    done
    unset AWK_COLOR_INCREMENT_AMOUNT_ONE AWK_COLOR_INCREMENT_AMOUNT_TWO AWK_COLOR_INCREMENT_AMOUNT_THREE AWK_COLOR_INCREMENT_AMOUNT_FOUR HEATMAP_BLUE HEATMAP_GREEN HEATMAP_YELLOW HEATMAP_ORANGE HEATMAP_RED HEATMAP_RESET_COLOR SA_FILE
}
print_master_heatmap() {
    eval paste mem-commit_heatmap.time_column1 mem-commit_heatmap.sa_date_{$TOTAL_DAYS..0} #| column -t
    unset TOTAL_DAYS
    rm -f ./mem-commit_heatmap*
}
# # # # #
# "Main"
pushd /tmp >/dev/null
print_header
user_selects_length_of_heatmap
create_heatmap_for_each_day_of_inquiry
create_time_column_for_heatmap
print_master_heatmap
popd >/dev/null

#!/bin/bash
# Prints a heatmap on received|transmitted packets|bytes per second per interface as chosen by user and as reported by sar.
# Maintainer: Derek Jenkins -- derektjenkins@gmail.com
# # # # #
# Global Vars
# -----------
E_BADARGS=85
ARGS=0
# # # # #
# Functions
# ---------
usage() {
    echo "Usage: $(basename "$0")"
    echo "Note: $(basename $0) takes no arguments. After running the script, provide numerical answers to the menu selections."
    exit "${E_BADARGS}"
}
print_header() {
    echo "==========================================================="
    echo "============ :: network interface heatmap :: =============="
    echo "==========================================================="
    echo "=== errored packets: ======================================"
    echo "=== collisions | dropped | malformed ======================"
    echo "==========================================================="
    echo "==== general traffic: ====================================="
    echo "==== packets | bytes ======================================"
    echo "============================== rx | tx :: rate: per second "
    echo "==========================================================="
}
user_selects_length_of_heatmap() {
    echo
    read -p "How many days do you want to include in this heatmap? " USER_SELECTED_DAYS
    TOTAL_DAYS=$((${USER_SELECTED_DAYS}-1))
    unset USER_SELECTED_DAYS
}
user_selects_interface_for_heatmap() {
    # # # # #
    #  This function sets $INTERFACE_CHOICE_FROM_SELECT, which comes
    #+ into play in the grep in the create_heatmap_for_each_day_of_inquiry() func.
    #  So -- this can be tested by echoing said variable (before it's `unset`)
    # # # # #
    INTERFACE_SELECT_OPTIONS=($(ip l | grep -v "link\/" | cut -d: -f2))
    echo -e "\nWhich interface do you want to run this heatmap against?"
    local PS3="Enter choice: "
    select INTERFACE_CHOICE_FROM_SELECT in "${INTERFACE_SELECT_OPTIONS[@]}"; do
        case "${INTERFACE_CHOICE_FROM_SELECT}" in
            lo)
            break
            ;;
            em*)
            break
            ;;
            eth*)
            break
            ;;
            virb*)
            break
            ;;
            *)
            echo "We do not have that option. Try again."
            return 2&> /dev/null
            ;;
        esac
    done
    unset INTERFACE_SELECT_OPTIONS
}
user_selects_errored_or_normal_traffic() {
    local FIRST_SAR_OPTIONS=( "interface errors" "interface traffic" )
    echo -e "\nDo you want to inspect interface errors or overall interface traffic?"
    local PS3="Enter choice: "
    echo
    select FIRST_SAR_CHOICE in "${FIRST_SAR_OPTIONS[@]}"; do
        case "${FIRST_SAR_CHOICE}" in
            "interface errors")
            SAR_TYPE="EDEV"
            user_selects_error_heatmap_sar_column
            break
            ;;
            "interface traffic")
            SAR_TYPE="DEV"
            user_selects_normal_heatmap_sar_column
            break
            ;;
            *)
            echo "We don't have that option. Try again."
            return 2&> /dev/null
            ;;
        esac
    done
}
user_selects_error_heatmap_sar_column() {
    local HEATMAP_SELECT_OPTIONS=( "bad packets received" "errors while transmitting packets" "collisions while transmitting packets" "received packets dropped because of a lack of space in buffers" "transmitted packets dropped because of a lack of space in buffers" "carrier-errors while transmitting packets" "frame alignment errors on received packets" "FIFO overrun errors received packets" "FIFO overrun errors on transmitted packets" )
    echo -e "\nWhich sort of interface errors do you want to heatmap?"
    local PS3="Enter choice: "
    echo
    select HEATMAP_TYPE_CHOICE in "${HEATMAP_SELECT_OPTIONS[@]}"; do
        case "${HEATMAP_TYPE_CHOICE}" in
            "bad packets received")
            HEATMAP_SAR_COLUMN="4"
            break
            ;;
            "errors while transmitting packets")
            HEATMAP_SAR_COLUMN="5"
            break
            ;;
            "collisions while transmitting packets")
            HEATMAP_SAR_COLUMN="6"
            break
            ;;
            "received packets dropped because of a lack of space in buffers")
            HEATMAP_SAR_COLUMN="7"
            break
            ;;
            "transmitted packets dropped because of a lack of space in buffers")
            HEATMAP_SAR_COLUMN="8"
            break
            ;;
            "carrier-errors while transmitting packets")
            HEATMAP_SAR_COLUMN="9"
            break
            ;;
            "frame alignment errors on received packets")
            HEATMAP_SAR_COLUMN="10"
            break
            ;;
            "FIFO overrun errors received packets")
            HEATMAP_SAR_COLUMN="11"
            break
            ;;
            "FIFO overrun errors on transmitted packets")
            HEATMAP_SAR_COLUMN="12"
            break
            ;;
            *)
            "We don't have that option. Try again."
            return 2&> /dev/null
            ;;
        esac
    done
    unset HEATMAP_SELECT_OPTIONS HEATMAP_TYPE_CHOICE
}
user_selects_normal_heatmap_sar_column() {
    local HEATMAP_SELECT_OPTIONS=( "packets received" "packets transmitted" "bytes received" "bytes transmitted" )
    echo -e "\nWhich traffic metric do you want to heatmap?"
    local PS3="Enter choice: "
    echo
    select HEATMAP_TYPE_CHOICE in "${HEATMAP_SELECT_OPTIONS[@]}"; do
        case "${HEATMAP_TYPE_CHOICE}" in
            "packets received")
            HEATMAP_SAR_COLUMN="4"
            break
            ;;
            "packets transmitted")
            HEATMAP_SAR_COLUMN="5"
            break
            ;;
            "bytes received")
            HEATMAP_SAR_COLUMN="6"
            break
            ;;
            "bytes transmitted")
            HEATMAP_SAR_COLUMN="7"
            break
            ;;
            *)
            "We don't have that option. Try again."
            return 2&> /dev/null
            ;;
        esac
    done
    unset HEATMAP_SELECT_OPTIONS HEATMAP_TYPE_CHOICE
}
set_outliers_and_mean_for_the_heatmap_increments() {
    # the awk print $3 before >> should shed the timestamps, leaving us with a single column of sar data
    for GATHERING_REPORT_INCREMENTS in $(eval echo {$TOTAL_DAYS..0}); do
        the_sar_command | grep "${INTERFACE_CHOICE_FROM_SELECT}" | awk_that_gets_the_required_column | awk '{print $3}' >> interface_heatmap.initial-report-to-set-scope
    done
    sort -n interface_heatmap.initial-report-to-set-scope | uniq > interface_heatmap.sorted-report-to-set-scope
    unset HIGHEST_REPORT_ENTRY LOWEST_REPORT_ENTRY TOTAL_REPORT_ENTRY_DISTANCE MEAN_REPORT_ENTRY
}
# These next few funcs are used in create_heatmap_for_each_day_of_inquiry()
the_sar_command() {
    sar -n "${SAR_TYPE}" -f "$(date -d "${SA_FILE} days ago" +/var/log/sa/sa%d)"
}
awk_that_gets_the_required_column() {
    # this awk script simply tests input to ensure that its output contains only rows whose values on columns $1 and $6 begin with timestamps or digits. This effectively removes the header and footer that sar provides (so that our output only contains the values for the resources we're interested in).
    # also note that I "cheated" awk here -- the $ before user_selected_column is there _only_ to preface the number that user_selected_column represents. awk then interprets that literally as $4 (or whatever number user selects) so that it can essentially perform a normal awk on that column.
    awk -v user_selected_column="${HEATMAP_SAR_COLUMN}" '$1 ~ /^[0-2][0-9]:/ && $6 ~ /^[0-9]/ \
    {
        print $1, $2, $user_selected_column
    }'
    unset HEATMAP_SAR_COLUMN
}
properly_format_the_required_column() {
    sed 's/:/ /g' | awk '{print $1"  " $2, $4, $5}' | sed -e 's/  /:/g' -e 's/ AM/AM/g' -e 's/ PM/PM/g'
}
join_the_sar_data_with_time_column_plus_na() {
    # When we print $2 at the end, we have a single column with the sar data or "N/A" (where sar didn't report).
    awk -F" " 'FNR==NR{a[$1]=$0;next}{if($1 in a){print a[$1];} else {print;}}' interface_heatmap.sa_date_"${SA_FILE}" interface_heatmap.time_column1_plus_na | awk '{print $2}'
}
awk_that_colorizes_each_day_of_inquiry() {
    # May choose to use this again if I can figure out a usable header.
    # else if ($1 ~ "Day")
    #     {
    #         printf reset_color
    #     }
    # Since red is the only non-bold color here, consider using all non-bold colors.
    local HEATMAP_BLUE="\033[1;34m"
    local HEATMAP_GREEN="\033[1;32m"
    local HEATMAP_YELLOW="\033[1;33m"
    local HEATMAP_ORANGE="\033[1;31m"
    local HEATMAP_RED="\033[0;31m"
    local HEATMAP_RESET_COLOR="\033[0;0m"
    # This gets numerical scope of data, as well as a mean. This is used to establish the next set of vars.
    local HIGHEST_REPORT_ENTRY=$(tail -1 interface_heatmap.sorted-report-to-set-scope)
    local LOWEST_REPORT_ENTRY=$(head -1 interface_heatmap.sorted-report-to-set-scope)
    local TOTAL_REPORT_ENTRY_DISTANCE=$(python -c "print ${HIGHEST_REPORT_ENTRY} - ${LOWEST_REPORT_ENTRY}")
    local MEAN_REPORT_ENTRY=$(python -c "print ${TOTAL_REPORT_ENTRY_DISTANCE} / 5 ")
    # This is where I set the increments used to separate the colored sections of the heatmap. Called in awk_that_colorizes_each_day_of_inquiry().
    # Consider not using the extra vars here -- rather, immediately set the awk -v vars with the output of the python math.
    local AWK_COLOR_INCREMENT_AMOUNT_ONE=$(python -c "print ${LOWEST_REPORT_ENTRY} + ${MEAN_REPORT_ENTRY}")
    local AWK_COLOR_INCREMENT_AMOUNT_TWO=$(python -c "print ${AWK_COLOR_INCREMENT_AMOUNT_ONE} + ${MEAN_REPORT_ENTRY}")
    local AWK_COLOR_INCREMENT_AMOUNT_THREE=$(python -c "print ${AWK_COLOR_INCREMENT_AMOUNT_TWO} + ${MEAN_REPORT_ENTRY}")
    local AWK_COLOR_INCREMENT_AMOUNT_FOUR=$(python -c "print ${AWK_COLOR_INCREMENT_AMOUNT_THREE} + ${MEAN_REPORT_ENTRY}")
    awk -v color_increment_amount_one="$AWK_COLOR_INCREMENT_AMOUNT_ONE" \
        -v blue="$HEATMAP_BLUE" \
        -v color_increment_amount_two="$AWK_COLOR_INCREMENT_AMOUNT_TWO" \
        -v green="$HEATMAP_GREEN" \
        -v color_increment_amount_three="$AWK_COLOR_INCREMENT_AMOUNT_THREE" \
        -v yellow="$HEATMAP_YELLOW" \
        -v color_increment_amount_four="$AWK_COLOR_INCREMENT_AMOUNT_FOUR" \
        -v orange="$HEATMAP_ORANGE" \
        -v red="$HEATMAP_RED" \
        -v reset_color="$HEATMAP_RESET_COLOR" \
        '{
            if ($1 == "N/A")
            {
                printf reset_color
            }
        else if ($1 <= color_increment_amount_one)
            {
                printf blue
            }
        else if ($1 <= color_increment_amount_two)
            {
                printf green
            }
        else if ($1 <= color_increment_amount_three)
            {
                printf yellow
            }
        else if ($1 <= color_increment_amount_four)
            {
                printf orange
            }
        else if ($1 > color_increment_amount_four)
            {
                printf red
            }
            print $1 reset_color
        }' interface_heatmap_data-with-na.sa_date_"${SA_FILE}" > interface_heatmap.sa_date_"${SA_FILE}"
}
create_time_column_plus_na() {
    # we use this in case sar has gaps in reporting. we call it when we create the "SA_FILE" reports.
    for TIME_INCREMENT in {1..143}; do
        date -u -d"12am+$((${TIME_INCREMENT}*10))mins" +"%I:%M%p%tN/A" >> interface_heatmap.time_column1_plus_na
    done
    unset TIME_INCREMENT
}
create_heatmap_for_each_day_of_inquiry() {
    # we use create_time_column_plus_na() so that we wind up with "N/A" fields where sar wasn't reporting. we use the file this func creates to join with each SA_FILE.
    create_time_column_plus_na
    for SA_FILE in $(eval echo {$TOTAL_DAYS..0}); do
        the_sar_command | grep "${INTERFACE_CHOICE_FROM_SELECT}" | awk_that_gets_the_required_column | properly_format_the_required_column > interface_heatmap.sa_date_"${SA_FILE}"
        join_the_sar_data_with_time_column_plus_na > interface_heatmap_data-with-na.sa_date_"${SA_FILE}"
        awk_that_colorizes_each_day_of_inquiry
    done
    unset AWK_COLOR_INCREMENT_AMOUNT_ONE AWK_COLOR_INCREMENT_AMOUNT_TWO AWK_COLOR_INCREMENT_AMOUNT_THREE AWK_COLOR_INCREMENT_AMOUNT_FOUR HEATMAP_BLUE HEATMAP_GREEN HEATMAP_YELLOW HEATMAP_ORANGE HEATMAP_RED HEATMAP_RESET_COLOR INTERFACE_CHOICE_FROM_SELECT SA_FILE SAR_TYPE
}
create_time_column_for_heatmap() {
    for TIME_INCREMENT in {1..143}; do
        date -u -d"12am+$((${TIME_INCREMENT}*10))mins" +"%I:%M%p%t" >> interface_heatmap.time_column1
    done
    unset TIME_INCREMENT
}
print_master_heatmap() {
    echo
    eval paste interface_heatmap.time_column1 interface_heatmap.sa_date_{$TOTAL_DAYS..0} #| column -t
    unset TOTAL_DAYS
}
remove_temp_files() {
    rm -f ./interface_heatmap*
    unset E_BADARGS ARGS
}
# # # # #
# "Main"
# ------
# Test: if $ARGS -ne to $#, run the usage function.
[[ $# -eq "${ARGS}" ]] || usage
pushd /tmp >/dev/null
print_header
user_selects_length_of_heatmap
user_selects_interface_for_heatmap
user_selects_errored_or_normal_traffic
set_outliers_and_mean_for_the_heatmap_increments
create_heatmap_for_each_day_of_inquiry
create_time_column_for_heatmap
print_master_heatmap
remove_temp_files
popd >/dev/null
#!/bin/bash
# prints heatmap on load average intervals from sar; 1, 5, 0r 15 min intervals as provided by the user.
# this divides `nproc` by 5 so that the 5 colors of the heatmap can be accurately relative to the system that this runs on.
print_header() {
    echo "=================================================="
    echo "============== load average heatmap =============="
    echo "==== 1, 5, 15 min avgs : per 10 min intervals ===="
    echo "=================================================="
}
show_user_number_of_cores() {
    echo -e "\nThe number of CPU cores on your system is helpful in assesing load. This script does take the number of processors on your system\ninto consideration and colorizes the heatmap according to those numbers.\n\nTo be clear, the number of CPU cores on your system is: $(nproc)"
}
create_time_column_for_heatmap() {
    for TIME_INCREMENT in {1..143}; do
        date -u -d"12am+$((${TIME_INCREMENT}*10))mins" +"%I:%M%p%t" >> load-avg_heatmap.time_column1
    done
    unset TIME_INCREMENT
}
create_time_column_plus_na() {
    # we use this in case sar has gaps in reporting. we call it when we create the "SA_FILE" reports.
    for TIME_INCREMENT in {1..143}; do
        date -u -d"12am+$((${TIME_INCREMENT}*10))mins" +"%I:%M%p%tN/A" >> load-avg_heatmap.time_column1_plus_na
    done
    unset TIME_INCREMENT
}
user_selects_type_of_heatmap() {
    local HEATMAP_SELECT_OPTIONS=( "1 minute averages" "5 minute averages" "15 minute averages" )
    echo -e "\nWhich time interval do you want this heatmap to report?\n"
    local PS3="Enter choice: "
    echo
    select HEATMAP_TYPE_CHOICE in "${HEATMAP_SELECT_OPTIONS[@]}"; do
        case "${HEATMAP_TYPE_CHOICE}" in
            "1 minute averages")
            HEATMAP_SAR_COLUMN="5"
            break
            ;;
            "5 minute averages")
            HEATMAP_SAR_COLUMN="6"
            break
            ;;
            "15 minute averages")
            HEATMAP_SAR_COLUMN="7"
            break
            ;;
            *)
            "We don't have that option. Try again."
            return 2&> /dev/null
            ;;
        esac
    done
    unset HEATMAP_SELECT_OPTIONS HEATMAP_TYPE_CHOICE
}
user_selects_length_of_heatmap() {
    echo
    read -p "How many days do you want to include in this heatmap? " USER_SELECTED_DAYS
    echo
    TOTAL_DAYS=$((${USER_SELECTED_DAYS}-1))
    unset USER_SELECTED_DAYS
}
# These next few funcs are used in create_heatmap_for_each_day_of_inquiry() (where $SA_FILE gets defined).
the_sar_command() {
    sar -q -f "$(date -d "${SA_FILE} days ago" +/var/log/sa/sa%d)"
}
awk_that_gets_the_required_column() {
    # this awk script simply tests input to ensure that its output contains only rows whose values on columns $1 and $6 begin with timestamps or digits. This effectively removes the header and footer that sar provides (so that our output only contains the values for the resources we're interested in).
    # also note that I "cheated" awk here -- the $ before user_selected_column is there _only_ to preface the number that user_selected_column represents. awk then interprets that literally as $4 (or whatever number user selects) so that it can essentially perform a normal awk on that column.
    awk -v user_selected_column="${HEATMAP_SAR_COLUMN}" '$1 ~ /^[0-2][0-9]:/ && $6 ~ /^[0-9]/ \
    {
        print $1, $2, $user_selected_column
    }'
    unset HEATMAP_SAR_COLUMN
}
properly_format_the_required_column() {
    sed 's/:/ /g' | awk '{print $1"  " $2, $4, $5}' | sed -e 's/  /:/g' -e 's/ AM/AM/g' -e 's/ PM/PM/g'
}
join_the_sar_data_with_time_column_plus_na() {
    # When we print $2 at the end, we have a single column with the sar data or "N/A" (where sar didn't report).
    awk -F" " 'FNR==NR{a[$1]=$0;next}{if($1 in a){print a[$1];} else {print;}}' load-avg_heatmap.sa_date_"${SA_FILE}" load-avg_heatmap.time_column1_plus_na | awk '{print $2}'
}
awk_that_colorizes_each_day_of_inquiry() {
    # May choose to use this again if I can figure out a usable header.
    # else if ($1 ~ "Day")
    #     {
    #         printf reset_color
    #     }
    # Since red is the only non-bold color here, consider using all non-bold colors.
    local HEATMAP_BLUE="\033[1;34m"
    local HEATMAP_GREEN="\033[1;32m"
    local HEATMAP_YELLOW="\033[1;33m"
    local HEATMAP_ORANGE="\033[1;31m"
    local HEATMAP_RED="\033[0;31m"
    local HEATMAP_RESET_COLOR="\033[0;0m"
    local AWK_COLOR_INCREMENT_AMOUNT_ONE=$(python -c "print $(nproc).00 / 5.00")
    local AWK_COLOR_INCREMENT_AMOUNT_TWO=$(python -c "print ${AWK_COLOR_INCREMENT_AMOUNT_ONE} * 2")
    local AWK_COLOR_INCREMENT_AMOUNT_THREE=$(python -c "print ${AWK_COLOR_INCREMENT_AMOUNT_ONE} * 3")
    local AWK_COLOR_INCREMENT_AMOUNT_FOUR=$(python -c "print ${AWK_COLOR_INCREMENT_AMOUNT_ONE} * 4")
    awk \
    -v color_increment_amount_one="$AWK_COLOR_INCREMENT_AMOUNT_ONE" \
    -v blue="$HEATMAP_BLUE" \
    -v color_increment_amount_two="$AWK_COLOR_INCREMENT_AMOUNT_TWO" \
    -v green="$HEATMAP_GREEN" \
    -v color_increment_amount_three="$AWK_COLOR_INCREMENT_AMOUNT_THREE" \
    -v yellow="$HEATMAP_YELLOW" \
    -v color_increment_amount_four="$AWK_COLOR_INCREMENT_AMOUNT_FOUR" \
    -v orange="$HEATMAP_ORANGE" \
    -v red="$HEATMAP_RED" \
    -v reset_color="$HEATMAP_RESET_COLOR" \
    '{
        if ($1 == "N/A")
        {
            printf reset_color
        }
    else if ($1 <= color_increment_amount_one)
        {
            printf blue
        }
    else if ($1 <= color_increment_amount_two)
        {
            printf green
        }
    else if ($1 <= color_increment_amount_three)
        {
            printf yellow
        }
    else if ($1 <= color_increment_amount_four)
        {
            printf orange
        }
    else if ($1 > color_increment_amount_four)
        {
            printf red
        }
        print $1 reset_color
    }' load-avg_heatmap_data-with-na.sa_date_"${SA_FILE}" > load-avg_heatmap.sa_date_"${SA_FILE}"

    unset HEATMAP_BLUE HEATMAP_GREEN HEATMAP_YELLOW HEATMAP_ORANGE HEATMAP_RED HEATMAP_RESET_COLOR AWK_COLOR_INCREMENT_AMOUNT_ONE AWK_COLOR_INCREMENT_AMOUNT_TWO AWK_COLOR_INCREMENT_AMOUNT_THREE AWK_COLOR_INCREMENT_AMOUNT_FOUR
}
create_heatmap_for_each_day_of_inquiry() {
    # we use create_time_column_plus_na() so that we wind up with "N/A" fields where sar wasn't reporting. we use the file this func creates to join with each SA_FILE.
    create_time_column_plus_na
    for SA_FILE in $(eval echo {$TOTAL_DAYS..0}); do
        the_sar_command | awk_that_gets_the_required_column | properly_format_the_required_column > load-avg_heatmap.sa_date_"${SA_FILE}"
        join_the_sar_data_with_time_column_plus_na > load-avg_heatmap_data-with-na.sa_date_"${SA_FILE}"
        # sed -i "1iDay_${SA_FILE}" load-avg_heatmap_data-with-na.sa_date_"${SA_FILE}"
        awk_that_colorizes_each_day_of_inquiry
    done
    unset SA_FILE
}
print_master_heatmap() {
    eval paste load-avg_heatmap.time_column1 load-avg_heatmap.sa_date_{$TOTAL_DAYS..0} #| column -t
    unset TOTAL_DAYS
    rm -f ./load-avg_heatmap*
}
# # # # #
# "Main"
pushd /tmp >/dev/null
print_header
show_user_number_of_cores
user_selects_type_of_heatmap
user_selects_length_of_heatmap
create_heatmap_for_each_day_of_inquiry
create_time_column_for_heatmap
print_master_heatmap
popd >/dev/null
